﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MicroTemplate.Services.MicroTemplate;

namespace MicroTemplate.RESTApi.Installers
{
    public static class ServicesInstaller
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IMicroService, MicroService>();
            return services;
        }
    }
}
