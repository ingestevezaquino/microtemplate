﻿using Microsoft.AspNetCore.Mvc;
using MicroTemplate.Services.Common;
using MicroTemplate.Services.MicroTemplate;
using System.Threading.Tasks;

namespace MicroTemplate.RESTApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TestController : ControllerBase
    {

        private readonly IMicroService _microService;

        public TestController(IMicroService microService)
        {
            _microService = microService;
        }

        [HttpGet]
        [Route("main")]
        public async Task<ActionResult<MicroResponse<string>>> Get()
        {
            var response = await _microService.Test();
            return response;
        }
    }
}
