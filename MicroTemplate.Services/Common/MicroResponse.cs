﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroTemplate.Services.Common
{
    public class MicroResponse
    {
        public bool Succeeded => !Errors.Any();
        public List<Error> Errors { get; set; } = new List<Error>();
    }

    public class MicroResponse<T> : MicroResponse where T : class
    {
        public T Value { get; set; }
    }
}
