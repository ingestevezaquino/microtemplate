﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroTemplate.Services.Common
{
    public class Error
    {
        public Error(ErrorCode code, string message)
        {
            Code = code;
            Message = message;
        }

        public ErrorCode Code { get; set; }
        public string Message { get; set; }
    }
}
