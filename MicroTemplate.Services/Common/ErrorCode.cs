﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MicroTemplate.Services.Common
{
    public enum ErrorCode
    {
        BadRequest = 400,
        NotFound = 404,
        InternalServerError = 500,
        BadGateway = 502,
        RemoteServerError = 900
    }
}
