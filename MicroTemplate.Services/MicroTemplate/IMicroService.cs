﻿using MicroTemplate.Services.Common;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MicroTemplate.Services.MicroTemplate
{
    public interface IMicroService
    {
        Task<MicroResponse<string>> Test();
    }
}
