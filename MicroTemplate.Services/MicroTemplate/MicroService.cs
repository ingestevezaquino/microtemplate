﻿using MicroTemplate.Services.Common;
using System;
using System.Threading.Tasks;

namespace MicroTemplate.Services.MicroTemplate
{
    public class MicroService : IMicroService
    {
        public async Task<MicroResponse<string>> Test()
        {
            var response = new MicroResponse<string>();

            Random dice = new Random();
            int roll = dice.Next(1, 7);

            bool result = roll % 2 == 0 ? true : false;

            if (result)
            {
                await Task.Delay(1000);
                response.Value = $"Positive: {roll} is even.";
            } 
            else 
            {
                await Task.Delay(1000);
                response.Errors.Add(new Error(ErrorCode.InternalServerError, $"We can not process an odd number ({roll})."));
            }

            return response;
        }
    }
}
